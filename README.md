# Flash Cards

A simple one file flash card application. 

<img src="flash-cards.png" height="400px">

You can load an input file with flash card data where each cards contents are on a single line with the question and answer separated by ` = `. 

## Features

- Dark and light modes
- Shuffle mode - load cards in random order
- Loop mode - keep loading cards when you reach the end of the stack
- You can choose to reverse the question and answers as well.